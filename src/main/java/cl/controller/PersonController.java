package cl.controller;

import cl.domain.Person;
import cl.repos.PersonRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;


@Controller
@Transactional
public class PersonController {

    @Autowired
    private PersonRepo personRepo;

    @GetMapping("/")
    public String greeting(Map<String, Object> model) {
        return "greeting";
    }

    @GetMapping("/mainPerson")
    public String p(Map<String, Object> model) {
        Iterable<Person> persons = personRepo.findAll();
        model.put("persons", persons);
        return "mainPerson";
    }
    @GetMapping("/infoPerson")
    public String inF(Map<String, Object> model) {
        return "infoPerson";
    }

    @PostMapping("/mainPerson")
    public String add(@RequestParam String name, @RequestParam String surname,
                      @RequestParam String patronymic,@RequestParam String age,
                      @RequestParam String salary,@RequestParam String email,
                      @RequestParam String work,Map<String, Object> model) {
        if(email.endsWith(".ru")== true || email.endsWith(".com")== true){
            Person person = new Person(name, surname, patronymic, Integer.parseInt(age), Integer.parseInt(salary), email, work);
            personRepo.save(person);
            try(FileWriter writer = new FileWriter("Users.txt", true)) {
                writer.write(name + " ");
                writer.write(surname+ " ");
                writer.write(patronymic+ " ");
                writer.write(age + " ");
                writer.write(salary + " ");
                writer.write(work + " ");
                writer.write(email + " ");
                writer.append('\n');
                writer.flush();
            }
            catch(IOException ex){

                System.out.println(ex.getMessage());
            }
            Iterable<Person> persons = personRepo.findAll();
            model.put("persons", persons);
            return "mainPerson";
        }else{
            return "errorAdd";
        }

    }

    @PostMapping("deleted")
    public String del(@RequestParam String name, Map<String, Object> model) {
        Iterable<Person> persons;

        if (name != null && !name.isEmpty()) {
            personRepo.deleteByName(name);
            persons=personRepo.findAll();
        } else {
            persons = personRepo.findAll();
        }
        model.put("persons", persons);

        return "mainPerson";
    }
    @PostMapping("deletedAll")
    public String delAll( Map<String, Object> model) {
            personRepo.deleteAll();
            model.clear();

        return "mainPerson";
    }


    @PostMapping("findPerson")
    public String filter(@RequestParam String name, @RequestParam String surname,Map<String, Object> model) {
        Iterable<Person> persons;

        if (name != null && !name.isEmpty() && surname != null && !surname.isEmpty()) {
            persons = personRepo.findByNameAndSurname(name, surname);
            model.put("persons", persons);
            return "infoPerson";
        } else {
            return "errorFindUser";
        }

    }

    @PostMapping("update")
    public String updatePerson(@RequestParam String id, @RequestParam String name,Map<String, Object> model) {
        Iterable<Person> persons;
        if (id != null && !name.isEmpty()) {
            personRepo.updateById(Integer.parseInt(id), name);
        }
            return "mainPerson";
    }

    @PostMapping("showAll")
    public String showAll(Map<String, Object> model) {
        Iterable<Person> persons;
        persons=personRepo.findAll();
        model.put("persons", persons);
        return "mainPerson";
    }




}
