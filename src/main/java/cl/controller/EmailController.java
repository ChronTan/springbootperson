package cl.controller;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Properties;

@Controller
public class EmailController {

    @GetMapping("/sendEmail")
    public String p(Map<String, Object> model) {
        return "sendEmail";
    }

    @PostMapping("sendEmail")
    public String send(@RequestParam String emailYour, @RequestParam String passwordYour,
                       @RequestParam String emailFriend, Map<String, Object> model) {

        SimpleMailMessage message = new SimpleMailMessage();
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(587);
        mailSender.setUsername(emailYour);
        mailSender.setPassword(passwordYour);

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");

        message.setTo(emailFriend);
        message.setSubject("Test Simple Email");
        message.setText("Hello, Im testing Simple Email");

        mailSender.send(message);
        return "sendEmail";
    }

}
