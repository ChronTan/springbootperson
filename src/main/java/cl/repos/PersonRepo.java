package cl.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import cl.domain.Person;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface PersonRepo extends CrudRepository<Person, Long> {

    List<Person> findByName(String name);
    List<Person> findByNameAndSurname(String name, String surname);
    List<Person> deleteByName(String name);

    @Transactional
    @Modifying
    @Query("UPDATE Person p SET p.name=:name WHERE p.id=:id")
    void updateById(@Param("id") int id, @Param("name") String name);

}
